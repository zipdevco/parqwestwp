<?php
if (!defined('ABSPATH')) exit;
global $blogvault;
global $bvNotice;
global $bvMigrateGuruAdminPage;
global $bvAppUrl;
global $bvNote;
$bvNotice = '';
$bvMigrateGuruAdminPage = 'migrateguru';
$bvNote = "Note: Please make sure you have a functional, publicly accessible WordPress site on the destination";
if (defined('BV_APP_URL')) {
	$bvAppUrl = BV_APP_URL;
} else {
	$bvAppUrl = 'https://mg.blogvault.net';
}

if (!function_exists('bvMigrationAddStyleSheet')) :
	function bvMigrationAddStyleSheet($hook) {
		wp_register_style('bvmig_style', plugins_url('css/style.css',__FILE__ ));
		wp_enqueue_style('bvmig_style');
	}
add_action( 'admin_enqueue_scripts','bvMigrationAddStyleSheet');
endif;

if (!function_exists('bvMigrateGuruAdminInitHandler')) :
	function bvMigrateGuruAdminInitHandler() {
		global $bvNotice, $blogvault, $bvMigrateGuruAdminPage, $bvAppUrl;
		global $sidebars_widgets;
		global $wp_registered_widget_updates;

		if (!current_user_can('activate_plugins'))
			return;

		if (isset($_REQUEST['bvnonce']) && wp_verify_nonce($_REQUEST['bvnonce'], "bvnonce")) {
			if (isset($_REQUEST['blogvaultkey']) && isset($_REQUEST['page']) && $_REQUEST['page'] == $bvMigrateGuruAdminPage) {
				if ((strlen($_REQUEST['blogvaultkey']) == 64)) {
					$keys = str_split($_REQUEST['blogvaultkey'], 32);
					$blogvault->updateKeys($keys[0], $keys[1]);
					bvActivateHandler();
					$bvNotice = "<b>Activated!</b> blogVault is now backing up your site.<br/><br/>";
					if (isset($_REQUEST['redirect'])) {
						$location = $_REQUEST['redirect'];
						wp_redirect($bvAppUrl."/migration/".$location);
						exit();
					}
				} else {
					$bvNotice = "<b style='color:red;'>Invalid request!</b> Please try again with a valid key.<br/><br/>";
				}
			}
		}
		if ($blogvault->getOption('bvActivateRedirect') === 'yes') {
			$blogvault->updateOption('bvActivateRedirect', 'no');
			wp_redirect($blogvault->bvAdminUrl($bvMigrateGuruAdminPage));
		}
	}
	add_action('admin_init', 'bvMigrateGuruAdminInitHandler');
endif;

if ( !function_exists('bvMigrateGuruAdminMenu') ) :
	function bvMigrateGuruAdminMenu() {
		global $bvMigrateGuruAdminPage;
		add_menu_page('Migrate Guru', 'Migrate Guru', 'manage_options', $bvMigrateGuruAdminPage, 'bvMigrateGuru');
		add_submenu_page( $bvMigrateGuruAdminPage, 'Bluehost', 'Bluehost', 'manage_options', 'mg-bluehost', 'bvBluehost');
		add_submenu_page( $bvMigrateGuruAdminPage, 'SiteGround', 'SiteGround', 'manage_options', 'mg-siteground', 'bvSiteground');
		add_submenu_page( $bvMigrateGuruAdminPage, 'HostGator', 'HostGator', 'manage_options', 'mg-hostgator', 'bvHostgator');
		add_submenu_page( $bvMigrateGuruAdminPage, 'GoDaddy', 'GoDaddy', 'manage_options', 'mg-godaddy', 'bvGodaddy');
		add_submenu_page( $bvMigrateGuruAdminPage, 'WPEngine', 'WPEngine', 'manage_options', 'mg-wpengine', 'bvWpengine');
		add_submenu_page( $bvMigrateGuruAdminPage, 'Flywheel', 'Flywheel', 'manage_options', 'mg-flywheel', 'bvFlywheel');
		add_submenu_page( $bvMigrateGuruAdminPage, 'Hostinger', 'Hostinger', 'manage_options', 'mg-hostinger', 'bvHostinger');
		add_submenu_page( $bvMigrateGuruAdminPage, 'cPanel', 'cPanel', 'manage_options', 'mg-cpanel', 'bvCpanel');
		add_submenu_page( $bvMigrateGuruAdminPage, 'FTP', 'FTP', 'manage_options', 'mg-ftp', 'bvFtp');
	}
	if (function_exists('is_multisite') && is_multisite()) {
		add_action('network_admin_menu', 'bvMigrateGuruAdminMenu');
	} else {
		add_action('admin_menu', 'bvMigrateGuruAdminMenu');
	}
endif;

if (!function_exists('mgActionLinks')) :
	function mgActionLinks($links, $file) {
		global $blogvault, $bvMigrateGuruAdminPage;

		if ( $file == plugin_basename( dirname(__FILE__).'/migrateguru.php' ) ) {
			$FAQ = '<a href="https://migrateguru.freshdesk.com/support/solutions/33000046011" target="_blank">'.__( 'FAQs' ).'</a>';
			array_unshift( $links, $FAQ );
			$Support = '<a href="https://wordpress.org/support/plugin/migrate-guru" target="_blank">'.__( 'Support' ).'</a>';
			array_unshift( $links, $Support );
			$RateUs = '<a href="https://wordpress.org/support/plugin/migrate-guru/reviews/?filter=5#new-post" target="_blank">'.__( 'Rate Us' ).'</a>';
			array_unshift( $links, $RateUs );
			$MigrateLink = '<a href="'.$blogvault->bvAdminUrl($bvMigrateGuruAdminPage).'">'.__( 'Migrate' ).'</a>';
			array_unshift( $links, $MigrateLink );
		}
		return $links;
	}
	add_filter('plugin_action_links', 'mgActionLinks', 10, 2);
endif;

if (!function_exists('bvFTPFormData')) :
	function bvFTPFormData($showport = false, $path = false) {
?>
		<input type="hidden" name="type" value="ftp" />
		<table id=ftp-form-table>
			<tr>
				<td>Email</td>
				<td><input type="text" name="email" placeholder="abc@gmail.com"></td>
				<td>Destination Site URL</td>
				<td><input type="text" name="newurl" placeholder="site.xyz.com"></td>
			</tr>
			<tr>
			<?php if ($path) :?>
				<td>Directroy Path</td>
				<td><input type="text" placeholder="/var/www/html/public_html/" name="path"></td>
			<?php endif;?>
			</tr>
			<tr>
				<td>FTP Host/Server Address</td>
				<td><input type="text" placeholder="123.456.789.101" name="address"></td>
			<?php if ($showport) :?>
				<td>FTP Port</td>
				<td><input type="text" placeholder="22" name="port"></td>
			<?php endif;?>	
			</tr>
			<tr>
				<td>FTP Username</td>
				<td><input type="text" placeholder="FTP username" name="username"></td>
				<td>FTP Password</td>
				<td><input type="password" placeholder="FTP password" name="passwd"></td>
			</tr>
		</table>
<?php
	}
endif;

if (!function_exists('bvFlywheelFormData')) :
	function bvFlywheelFormData() {
?>
		<input type="hidden" name="type" value="sftp" />
		<input type="hidden" name="address" value="sftp.flywheelsites.com" />
		<table id=flywheel-form-table>
			<tr>
				<td>Email</td>
				<td><input type="text" name="email" placeholder="abc@gmail.com"></td>
				<td>Destination Site URL</td>
				<td><input type="text" name="newurl" placeholder="http://example.flywheel.com"></td>
			</tr>
			<tr>
				<td>Flywheel URL</td>
				<td><input type="text" placeholder="https://app.getflywheel.com/user/site" name="flypath"></td>
			</tr>
			<tr>
				<td>sFTP Username</td>
				<td><input type="text" placeholder="sFTP username" name="username"></td>
				<td>sFTP Password</td>
				<td><input type="password" placeholder="sFTP password" name="passwd"></td>
			</tr>
		</table>
<?php
	}
endif;

if (!function_exists('bvSFTPFormData')) :
	function bvSFTPFormData($showport = false, $appfolder = false, $path = false) {
?>
		<input type="hidden" name="type" value="sftp" />
		<table id=sftp-form-table>
			<tr>
				<td>Email</td>
				<td><input type="text" name="email" placeholder="abc@gmail.com"></td>
				<td>Destination Site URL</td>
				<td><input type="text" name="newurl" placeholder="site.xyz.com"></td>
			</tr>
			<tr>
			<?php if ($appfolder) :?>
				<td>Application Folder Name</td>
				<td><input type="text" placeholder="appfoldername" name="appfolder"></td>
			<?php endif;?>
			<?php if ($path) :?>
				<td>Directroy Path</td>
				<td><input type="text" placeholder="/var/www/html/public_html/" name="path"></td>
			<?php endif;?>
			</tr>
			<tr>
				<td>sFTP Host/Server Address</td>
				<td><input type="text" placeholder="123.456.789.101" name="address"></td>
			<?php if ($showport) :?>
				<td>sFTP Port</td>
				<td><input type="text" placeholder="22" name="port"></td>
			<?php endif;?>	
			</tr>
			<tr>
				<td>sFTP Username</td>
				<td><input type="text" placeholder="sFTP username" name="username"></td>
				<td>sFTP Password</td>
				<td><input type="password" placeholder="sFTP password" name="passwd"></td>
				</tr>
		</table>
<?php
	}
endif;

if (!function_exists('bvAllFormData')) :
	function bvAllFormData() {
?>
		<input type="hidden" name="migrate" value="generic" />
		<table id=all-form-table>
			<tr>
				<td>Email</td>
				<td><input type="text" name="email" placeholder="abc@gmail.com"></td>
				<td>Destination Site URL</td>
				<td><input type="text" name="newurl" placeholder="site.xyz.com"></td>
			</tr>
			<tr>
				<td>FTP Type</td>
				<td>
					<input type="radio" name="type" value="ftp" checked> FTP
    			<input type="radio" name="type" value="sftp"> sFTP
					<input type="radio" name="type" value="ftps"> FTPs
				</td>
			</tr>
			<tr>
				<td>Directroy Path</td>
				<td><input type="text" placeholder="/var/www/html/public_html/" name="path"></td>
			</tr>
			<tr>
				<td>Host/Server Address</td>
				<td><input type="text" placeholder="123.456.789.101" name="address"></td>
				<td>Port</td>
				<td><input type="text" placeholder="22" name="port"></td>
			</tr>
			<tr>
				<td>Username</td>
				<td><input type="text" placeholder="username" name="username"></td>
				<td>Password</td>
				<td><input type="password" placeholder="password" name="passwd"></td>
			</tr>
		</table>
<?php
	}
endif;

if (!function_exists('bvHostingerFormData')) :
function bvHostingerFormData() {
?>
			<input type="hidden" name="type" value="ftp" />
			<table id=ftp-form-table>
				<tr>
				<td>Email</td>
				<td><input type="text" name="email" placeholder="abc@gmail.com"></td>
				<td>Destination Site URL</td>
				<td><input type="text" name="newurl" placeholder="site.xyz.com"></td>
				</tr>
				<tr>
				<td>Destination Server FTP IP </td>
				<td><input type="text" placeholder="ex. 123.456.789.101" name="address"></td>
				</tr>
				<tr>
				<td>FTP Username</td>
				<td><input type="text" placeholder="FTP username" name="username"></td>
				<td>FTP Password</td>
				<td><input type="password" placeholder="FTP password" name="passwd"></td>
				</tr>
			</table>
<?php }
endif;

if (!function_exists('bvCPanelFormData')) :
function bvCPanelFormData() {
?>
			<input type="hidden" name="type" value="ftp" />
			<table id=capenl-form-table>
				<tr>
				<td>Email</td>
				<td><input type="text" name="email" placeholder="abc@gmail.com"></td>
				<td>Destination Site URL</td>
				<td><input type="text" name="newurl" placeholder="site.xyz.com"></td>
				</tr>
				<tr>
				<td>Destination Server IP Address</td>
				<td><input type="text" placeholder="ex. 123.456.789.101" name="address"></td>
				</tr>
				<tr>
				<td>cPanel Username</td>
				<td><input type="text" placeholder="cPanel username" name="username"></td>
				<td>cPanel Password</td>
				<td><input type="password" placeholder="cPanel password" name="passwd"></td>
				</tr>
			</table>
<?php }
endif;

if (!function_exists('bvShowErrors')) :
	function bvShowErrors() {
		$error = NULL;
		if (array_key_exists('error', $_REQUEST)) {
			$error = $_REQUEST['error'];
		}
		if ($error == "email") {
			echo 'There is already an account with this email.';
		} else if ($error == "blog") {
			echo '<div style="padding-bottom:0.5%; color:red;"><p>Could not create an account. Please contact <a href="http://blogvault.net/contact/">blogVault Support</a><br />
				<font color="red">NOTE: We do not support automated migration of locally hosted sites..</font></p></div>';
		} else if (($error == "custom") && isset($_REQUEST['bvnonce']) && wp_verify_nonce($_REQUEST['bvnonce'], "bvnonce")) {
			if (isset($_REQUEST['auth_required_dest'])) {
				echo '<div style="padding-bottom:0.5%;color: red;"><p>Your destination site is passward protected. Please provide credentials in Advance Options</p></div>';
			}	else if (isset($_REQUEST['auth_required_source'])) {
				echo '<div style="padding-bottom:0.5%;color: red;"><p>Your source site is passward protected. Please provide credentials in Advance Options</p></div>';
			} else {
				echo '<div style="padding-bottom:0.5%;color: red;"><p>'.base64_decode($_REQUEST['message']).'</p></div>';
			}
		}
	}
endif;

if ( !function_exists('bvHeader')) :
	function bvHeader() {
		$page = $_GET['page'];
?>
		<h1><?php $_GET['page']; ?></h1>
		<div class="bv_inside_heading">
			<a href="http://migrateguru.com/"><img src="<?php echo plugins_url('img/migrateguru.png', __FILE__); ?>" /></a>
			<a class='poweredlogo' href="http://blogvault.net/"><img src="<?php echo plugins_url('img/logo.png', __FILE__); ?>" /></a>
			<ul class="breadcrumb">
<?php
		echo "<li><a href='" . admin_url() . "admin.php?page=migrateguru'>Home</a></li>";
		switch ($page) {
		case 'migrateguru':
			break;
		case 'mg-bluehost':
			echo "<li>Bluehost</li>";
			break;
		case 'mg-hostinger':
			echo "<li>Hostinger</li>";
			break;
		case 'mg-siteground':
			echo "<li>SiteGround</li>";
			break;
		case 'mg-hostgator':
			echo "<li>HostGator</li>";
			break;
		case 'mg-godaddy':
			echo "<li>GoDaddy</li>";
			break;
		case 'mg-wpengine':
			echo "<li>WP Engine</li>";
			break;
		case 'mg-flywheel':
			echo "<li>Flywheel</li>";
			break;
		case 'mg-cpanel':
			echo "<li>cPanel</li>";
			break;
		case 'mg-ftp':
			echo "<li>FTP</li>";
			break;
		}
?>
			</ul>
		</div>
<?php	}
endif;

if ( !function_exists('bvhelp') ) :
	function bvhelp() {
		echo '<div class="bvhelp">
			<ul class="bvheaderright">
			<li><a href="https://migrateguru.freshdesk.com/support/solutions/33000046011" target="_blank">FAQs</a></li>
			<li><a href="https://migrateguru.freshdesk.com/support/solutions/33000052052" target="_blank">Help Docs</a></li>
			<li><a href="https://wordpress.org/support/plugin/migrate-guru" target="_blank">Support</a></li>
			<li><a href="https://wordpress.org/support/plugin/migrate-guru/reviews/?filter=5#new-post" target="_blank">Rate us 5 stars</a></li>
			</ul>
			</div>';

	}
endif;

if ( !function_exists('bvAdvanceOptions') ) :
	function bvAdvanceOptions() {?>
		<br/>
		<br/>
		<button name="advance-options" id="advance-options-btn" class="button" onclick="bvShowHttpAuthForm(this.id); return false">Advanced Options</button>
		<hr/>
			
		<div id="advance-options-form" style="display:none">
			<table>
				<tr>
					<h3>HTTP Auth for Source Site</h3>
				</tr>
				<tr>
					<td>Username</td><td><input type="text" name="httpauth_src_user"></td>
				</tr>
				<tr>
					<td>Password</td><td><input type="password" name="httpauth_src_password"></td>
				</tr>
				<tr>
					<th><h3>HTTP Auth for Destination Site</h3></th>
				</tr>
				<tr>
					<td>Username</td><td><input type="text" name="httpauth_dest_user"></td>
				</tr>
				<tr>			
					<td>Password</td><td><input type="password" name="httpauth_dest_password"></td>
				</tr>
			</table>
		</div>
	<script>
	function bvShowHttpAuthForm(btnId) {
		var advance_op = document.getElementById('advance-options-form');
		if (btnId === 'advance-options-btn') {
			if (advance_op.style.display === 'none') {
				advance_op.style.display = 'block';
			} else {
				advance_op.style.display = 'none';
			}
		}
	}
	</script>
<?php		}
endif;

if ( !function_exists('bvHostingerBlurb') ) :
	function bvHostingerBlurb() { ?>
	<div>
	<p><b>To migrate to Hostinger</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>For your site server's IP address, log in to your Domain account on Hostinger → Account details → FTP Upload details.</li>
		<li>Enter your FTP username and password.</li>
	</ol>
For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000189545-how-to-move-your-site-to-hostinger-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvBlueHostBlurb') ) :
	function bvBlueHostBlurb() { ?>
	<div>
	<p><b>To migrate to BlueHost</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>For your site server's IP address, log in to your Bluehost account → Hosting → cPanel.</li>
		<li>Enter your cPanel username and password.</li>
	</ol>
<p>Your cPanel username is usually your domain name.<br>
Your cPanel password is the same password you use to login to your BlueHost account.<br>
For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000098221-to-migrate-a-site-to-bluehost-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvSiteGroundBlurb') ) :
	function bvSiteGroundBlurb() { ?>
	<div>
	<p><b>To migrate to SiteGround</b></p>
	<ol type="a">
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>For other details required, Click on ‘My Accounts</li>
		<p>Go to ‘Information & Settings’</p>
		<ol type="a">
			<li>‘Domain Name’ is your Destination URL</li>
			<li>‘Account IP’  is your server IP address</li>
			<li>‘cPanel Username’–– please insert this in the ‘cPanel username’ field</li>
			<li>Your ‘cPanel Password’ would have been mailed to you. If you do not remember the password then there is a button to the right of your cPanel username to reset your cPanel password.</li>
		</ol>
	</ol>	
		<p>For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000098337-to-migrate-a-site-to-siteground-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvHostGatorBlurb') ) :
	function bvHostGatorBlurb() { ?>
	<div>
	<p><b>To migrate to HostGator</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>Your domain name (destination site URL), destination server IP Address along with your cPanel username, and password are in the welcome mail you receive when you register an account with HostGator. The subject line of the email may be “HostGator.com :: Your Account Info”</li>
		<li>To find your domain name and server IP address, log into your cPanel account→ ‘General Information’</li>
	</ol>
	<p>For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000102671-to-migrate-a-site-to-hostgator-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvGoDaddyBlurb') ) :
	function bvGoDaddyBlurb() { ?>
	<div>
	<p><b>To migrate to GoDaddy</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>Log in to your GoDaddy account.</li>
		<ol>
			<li>Click ‘Hosting’.</li>
			<li>Next to the account you want to use, click ‘Manage’.</li>
			<li>From the ‘Settings’ section, click ‘view’ next to SFTP Users</li>
		</ol>
	</ol>
	<p>For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000098317-to-migrate-a-site-to-godaddy-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvWpEngineBlurb') ) :
	function bvWpEngineBlurb() { ?>
	<div>
	<p><b>To migrate to WP Engine</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>Log in to your WP Engine account</li>
		<ol>
			<li>Click on your site name –– This leads to the site ‘Overview’ page</li>
			<ol>
				<li>You should see the IP Address in the site Overview’ page</li>
				<li>You should also see the URL here</li>
			</ol>
			<li>In side menu options click sFTP Users</li>
			<ol>
				<li>This should reveal the sFTP host server name, sFTP username & Port</li>
			</ol>
			<li>If you don’t remember the password then you can create a new SFTP User altogether and enter those credentials here.</li>
		</ol>
	</ol>
	<p>For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000102664-to-migrate-a-site-to-wp-engine-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvFlywheelBlurb') ) :
	function bvFlywheelBlurb() { ?>
	<div>
	<p><b>To migrate to Flywheel</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>Flywheel URL –– this is the URL that appears when you log in to your Flywheel account, and click on the destination site.</li>
	</ol>
	<p>SFTP username –– Same as flywheel account username<br>
	SFTP password –– Same as your flywheel account password<br>
	For more details, <a href="https://migrateguru.freshdesk.com/support/solutions/articles/33000098227-to-migrate-a-site-to-flywheel-using-migrate-guru" target="_blank">Click here</a></p>
	</div>
<?php }
endif;

if ( !function_exists('bvCpanelBlurb') ) :
	function bvCpanelBlurb() { ?>
	<div>
	<p><b>To migrate using cPanel</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>cPanel credentials you use to log in to your hosting account’s cPanel dashboard.</li>
	</ol>
	<p>You would have been required to set this up when you signed up for an account with your web host.<br>
	Some web hosts even would have mailed it to you.<br>
	If you are unsure of what cPanel credentials are then please contact your webhost.<br></p>
	</div>
<?php }
endif;

if ( !function_exists('bvFtpBlurb') ) :
	function bvFtpBlurb() { ?>
	<div>
	<p><b>To migrate using FTP/sFTP/FTPs</b></p>
	<ol>
		<li>Enter the URL of the site you're moving to (Migrate Guru will move your site to this URL)</li>
		<li>FTP credentials are used to securely modify–– copy, add, delete; files on your site’s server.</li>
	</ol>		
	<p>Usually FTP details can be found your hosting account’s dashboard or cPanel dashboard.<br>
	Log in to your web host’s cPanel account and you should be able to find your FTP details there.<br>
	If you are unsure how to do this please contact your web host to learn more about your site’s FTP details.<br></p>
	</div>
<?php }
endif;

if ( !function_exists('bvHostinger') ) :
	function bvHostinger() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();
?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to Hostinger</h1>
		<div>
		<form id="mg-hostinger" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
      <input type="hidden" name="migrate" value="hostinger" />
<?php			
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);
		bvHostingerFormData();
		bvAdvanceOptions();
		?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
		</div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
			bvHostingerBlurb();
			bvhelp();
		?>
		</div>
		</div>
<?php	}
endif;

if ( !function_exists('bvBluehost') ) :
	function bvBluehost() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();
?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to Bluehost</h1>
		<div>
		<form id="mg-bluehost" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
      <input type="hidden" name="migrate" value="bluehost" />
<?php			
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);
		bvCPanelFormData();
		bvAdvanceOptions();
		?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
		</div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
			bvBlueHostBlurb();	
			bvhelp();
		?>
		</div>
		</div>
<?php	}
endif;

if ( !function_exists('bvSiteground') ) :
	function bvSiteground() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to Siteground</h1>
		<div>
		<form id="mg-siteground" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
      <input type="hidden" name="migrate" value="siteground" />
<?php			
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);
		bvCPanelFormData();
		bvAdvanceOptions();?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
		</div>
		</div>
		<div class='bv_selectedinside_column2'>
			<?php
				bvSiteGroundBlurb();
     	 	bvhelp();
    	?>
		</div>
		</div>
<?php	}
endif;

if ( !function_exists('bvHostgator') ) :
	function bvHostgator() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to Hostgator</h1>
		<div>
		<form id="mg-hostgator" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
      <input type="hidden" name="migrate" value="hostgator" />
<?php			
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);
		bvCPanelFormData();
		bvAdvanceOptions();?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
		</div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
				bvHostGatorBlurb();
				bvhelp();
		?>
		</div>
		</div>
<?php }
endif;

if ( !function_exists('bvGodaddy') ) :
	function bvGodaddy() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to GoDaddy</h1>
		<div>
		<form id="mg-godaddy" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
    	<input type="hidden" name="migrate" value="godaddy" />
<?php
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);
		bvFTPFormData();
		bvAdvanceOptions();?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
		</div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
			bvGoDaddyBlurb();
			bvhelp();
		?>
		</div>
		</div>
<?php }
endif;

if ( !function_exists('bvWpengine') ) :
	function bvWpengine() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to WP Engine</h1>
		<div>
		<form id="mg-wpengine" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
    	<input type="hidden" name="migrate" value="wpengine" />
<?php
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);	
		bvSFTPFormData();
		bvAdvanceOptions();?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
		</div>
		</div>
		<div class='bv_selectedinside_column2'>
			<?php
				bvWpEngineBlurb();
      	bvhelp();
    	?>
		</div>
		</div>
<?php	}
endif;

if ( !function_exists('bvFlywheel') ) :
	function bvFlywheel() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
		<div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to Flywheel</h1>
		<div>
		<form id="mg-flywheel" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
    	<input type="hidden" name="migrate" value="flywheel" />
<?php
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);		
		bvFlywheelFormData();
		bvAdvanceOptions();?>
			<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
    </div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
				bvFlywheelBlurb();
				bvhelp();
		?>
    </div>
    </div>
<?php	}
endif;

if ( !function_exists('bvCpanel') ) :
	function bvCpanel() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
    <div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site using cPanel</h1>
		<div>
		<form id="mg-cpanel" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
    	<input type="hidden" name="migrate" value="cpanel" />
<?php
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);		
		bvCPanelFormData();
		bvAdvanceOptions();?>
    	<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
    </div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
				bvCpanelBlurb();
				bvhelp();
		?>
    </div>
    </div>
<?php	}
endif;

if ( !function_exists('bvFtp') ) :
	function bvFtp() {
		global $blogvault, $bvNotice, $bvMigrateGuruAdminPage, $bvAppUrl, $bvNote;
?>
    <div class="bv_page_wide">
<?php
		bvHeader();
		bvShowErrors();?>
		<div class='bv_inside_column1'>
		<h1>Migrate site to Any other host using FTP/sFTP/FTPs details</h1>
		<div>
		<form id="mg-ftp" dummy=">" action="<?php echo $bvAppUrl; ?>/home/migrate" style="padding:0 2% 2em 1%;" method="post" name="signup">
			<input type="hidden" name="bvsrc" value="wpplugin" />
			<input type="hidden" name="origin" value="migrateguru" />
    	<input type="hidden" name="migrate" value="generic" />
<?php
		echo $blogvault->siteInfoTags($bvMigrateGuruAdminPage);		
		bvAllFormData();
		bvAdvanceOptions();?>
    	<input type='submit' value='Migrate'>
			<br/><i><?php echo $bvNote;?></i>
		</form>
    </div>
		</div>
		<div class='bv_selectedinside_column2'>
		<?php
				bvFtpBlurb();
				bvhelp();
		?>
    </div>
    </div>
<?php	}
endif;

if ( !function_exists('bvMigrateGuru') ) :
	function bvMigrateGuru() {
?>
		<div class="bv_wrap">
		<div class="bv_page_wide">
			<?php bvHeader(); ?>
			<div class="bv_inside_column1">
			<?php bvShowErrors() ?>
			<table id="hosts-table">
					<tr> <td colspan="3"> <h3 class="bv_title">Which webhost are you migrating to?<h3> </td> </tr>
					<tr>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
								<tr>
									<td><a id="bluehostbtn" href=<?php echo admin_url()?>admin.php?page=mg-bluehost><img class="bvhostlogo" id="bvbluehostlogo" src="<?php echo plugins_url('img/bluehost.png', __FILE__); ?>"></a></td>
									</tr>
								</table>
							</div>
						</td>
						<td>
              <div style="padding: 0px 10px 20px">
                <table>
                <tr>
                  <td><a id="hostingerbtn" href=<?php echo admin_url()?>admin.php?page=mg-hostinger><img class="bvhostlogo" id="bvhostingerlogo" src="<?php echo plugins_url('img/hostinger.png', __FILE__); ?>"></a></td>
                  </tr>
                </table>
              </div>
            </td>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
									<td><a id="sitegroundbtn" href=<?php echo admin_url()?>admin.php?page=mg-siteground><img class="bvhostlogo" id="bvsitegroundlogo" src="<?php echo plugins_url('img/siteground.png', __FILE__); ?>"></a></td>
									</tr>
								</table>
							</div>	
						</td>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
										<td><a id="hostgatorbtn" href=<?php echo admin_url()?>admin.php?page=mg-hostgator><img class="bvhostlogo" id="bvhostgatorlogo" src="<?php echo plugins_url('img/hostgator.png', __FILE__); ?>"></a></td>
									</tr>
									</table>
							</div>
						</td>
						</tr>
						<tr>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
										<td><a id="godaddybtn" href=<?php echo admin_url()?>admin.php?page=mg-godaddy><img class="bvhostlogo" id="bvgodaddylogo" src="<?php echo plugins_url('img/godaddy.png', __FILE__); ?>"></a></td>
									</tr>
									</table>
							</div>	
						</td>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
										<td><a id="wpenginebtn" href=<?php echo admin_url()?>admin.php?page=mg-wpengine><img class="bvhostlogo" id="bvwpenginelogo" src="<?php echo plugins_url('img/wpengine.png', __FILE__); ?>"></a></td>
									</tr>
									</table>
							</div>
						</td>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
										<td><a id="flywheelbtn" href=<?php echo admin_url()?>admin.php?page=mg-flywheel><img class="bvhostlogo" id="bvflywheellogo" src="<?php echo plugins_url('img/flywheel.png', __FILE__); ?>"></a></td>
									</tr>
								</table>
							</div>
						</td>
						</tr>
					</table>
					<hr/>
					<br>
					<table id="mg-ftp">
					<tr> <td colspan="3"> <h3 class='bv_title'>Don’t see your webhost in the list? Migrate your site via one of the following options<h3> </td> </tr>
					<tr>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
										<td><a id="cpanelbtn" href=<?php echo admin_url()?>admin.php?page=mg-cpanel><img class="bvhostlogo" id="bvcpanellogo" src="<?php echo plugins_url('img/cpanel.png', __FILE__); ?>"></a></td>
									</tr>
								</table>
							</div>
						</td>
						<td>
							<div style="padding: 0px 10px 20px">
								<table>
									<tr>
										<td><a id="ftpallmigbtn" href=<?php echo admin_url()?>admin.php?page=mg-ftp><img class="bvhostlogo" id="bvftpallmiglogo" src="<?php echo plugins_url('img/ftp.png', __FILE__); ?>"></a></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</div>
		<div class="bv_selectedinside_column2">
			<div>
				<iframe id="video" width="420" height="315" src="//www.youtube.com/embed/HBGcUdOhMcI?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<?php bvhelp(); ?>
		</div>
		</div>
<?php
	}
endif;